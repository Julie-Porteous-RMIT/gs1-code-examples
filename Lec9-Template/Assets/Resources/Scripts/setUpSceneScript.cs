﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class setUpSceneScript : MonoBehaviour
{
    private float pixelsToUnits;
    private GameObject thePlayer;

    void Awake()
    {
        thePlayer = Instantiate((GameObject)Resources.Load("Prefabs/Player"));

        pixelsToUnits = thePlayer.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
        Camera.main.orthographicSize = (Screen.height * 0.5f) / pixelsToUnits;

        float topLeftX = -((Screen.width * 0.5f) / pixelsToUnits) + thePlayer.GetComponent<SpriteRenderer>().sprite.bounds.size.x * 0.5f;
        float topLeftY = (Screen.height * 0.5f / pixelsToUnits) - thePlayer.GetComponent<SpriteRenderer>().sprite.bounds.size.y * 0.5f;
        thePlayer.transform.position = new Vector3(topLeftX, topLeftY, 0);
    }

}