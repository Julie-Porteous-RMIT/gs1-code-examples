﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnCoinScript : MonoBehaviour
{
    GameObject coinPrefab;

    // Start is called before the first frame update
    void Start()
    {
        coinPrefab = (GameObject)Instantiate(Resources.Load("Prefabs/GoldCoin"));
        coinPrefab.transform.position = new Vector3(0, 0, 0);
    }
}
