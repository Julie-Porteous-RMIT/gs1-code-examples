﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour
{
    private int score;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        score = GameObject.FindWithTag("Player").GetComponent<PlayerScript>().Score += 100;
        print($"Score: {score}");
        Destroy(gameObject);
    }
}
