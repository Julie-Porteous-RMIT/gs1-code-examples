﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class setUpSceneScript : MonoBehaviour
{
    private float pixelsToUnits;
    private GameObject thePlayer;
    private int numCoins = 2;

    void Awake()
    {
        thePlayer = Instantiate((GameObject)Resources.Load("Prefabs/Player"));

        pixelsToUnits = thePlayer.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
        Camera.main.orthographicSize = (Screen.height * 0.5f) / pixelsToUnits;

        // position player top left corner
        float topLeftX = -((Screen.width * 0.5f) / pixelsToUnits) + thePlayer.GetComponent<SpriteRenderer>().sprite.bounds.size.x * 0.5f;
        float topLeftY = (Screen.height * 0.5f / pixelsToUnits) - thePlayer.GetComponent<SpriteRenderer>().sprite.bounds.size.y * 0.5f;
        thePlayer.transform.position = new Vector3(topLeftX, topLeftY, 0);

        // position Portal at (0,0,0)
        GameObject portalTile = (GameObject)Instantiate(Resources.Load("Prefabs/BlackTile"));
        portalTile.transform.position = new Vector3(0,0, 0);

        // position Coins starting from (-3,1,0)
        int xPos = -3;
        for (int i = 0; i < numCoins; i++)
        {
            GameObject coinPrefab = (GameObject)Instantiate(Resources.Load("Prefabs/GoldCoin"));
            coinPrefab.transform.position = new Vector3(xPos, 1, 0);
            xPos++;
        }
    }
}