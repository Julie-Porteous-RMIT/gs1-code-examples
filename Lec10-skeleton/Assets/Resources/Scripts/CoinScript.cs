﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour
{
    private int score;

      private void OnTriggerEnter2D(Collider2D collision)
    {
        score = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript>().Score += 100;
        print($"SCORE: {score}");
        Destroy(gameObject);
    }
}
