﻿using System;
namespace GameSkeleton
{
    public class RunGame
    {
        // Fields
        // ======
        private bool _gameOver = false;
        private char _nextAction;
        private GameState _theGame;

        // Constructor
        // ===========
        public RunGame()
        {
            // new instance of GameState (see: lab5 exercise1-2)

            // display any player welcomes and instructions etc

            // set Percepts (what the player is next to) (see: lab4 exercise5)
            // print what the player is next to
        }

        // Properties
        // ==========
        public bool GameOver
        {
            // your code goes here
        }

        // Methods
        // =======
        // single method to run game
        public void run()
        {
            while (!GameOver)
            {
                // get _next Action (using static class Menu) see: lab5 exercise5

                switch (_nextAction)
                {
                    case 'g':
                    // cheat - print gameboard
                    case 'e':
                        // your code goes here
                        // (1) what is index of cell to East (see: lab5 exercise 3)
                        // (2) if its not -1
                        //      (a) if Empty or contains Treasure or Exit
                        //          can move to it
                        //          move the Player to it (see: lab5 exercise 3)
                        //          set the Percepts (what player is next to) (see: lab4 exercise5)
                        //      (b) its Hazard
                        //          GameOver and tell the player
                        // (3) if not GameOver
                        //     print Percepts (what the player is next to)(see: lab4 exercise5)
                        break;
                    case 'w':
                        // your code goes here - same steps as for East
                        break;
                    case 'n':
                        // your code goes here - same steps as for East
                        break;
                    case 's':
                        // your code goes here - same steps as for East
                        break;
                    case 'x':
                        // if AtExit
                        //      GameOver is true and print out total Treasure
                        // else
                        //      tell player they can only exit from exit square
                        break;
                }
            }
        }
    }
}
