﻿using System;
namespace RPGCharacter
{
    public class RPGCharacter
    {
        private string _name;
        private int _health;

        public RPGCharacter()
        {
            _name = "unknown";
            _health = 10;
        }
        public RPGCharacter(string name)
        {
            _name = name;
            _health = 10;
        }
        public RPGCharacter(int health)
        {
            _name = "unknown";
            _health = health;
        }
        public RPGCharacter(string name, int health)
        {
            name = _name;
            health = _health;
        }

        public string Name
        {
            get => _name;
            set => _name = value;  
        }

        public int Health
        {
            get { return _health; }
            set { _health = value; }
        }

        public void Print()
        {
            Console.WriteLine($"Char {_name} has health: {_health}");
        }
    }
}
