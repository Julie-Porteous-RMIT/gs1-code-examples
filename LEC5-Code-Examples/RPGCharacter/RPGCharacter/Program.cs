﻿using System;

namespace RPGCharacter
{
    class Program
    {
        static void Main(string[] args)
        {
            RPGCharacter char1 = new RPGCharacter();
            char1.Name = "Leia";
            char1.Health = 500;
            char1.Print();

            var char2 = new RPGCharacter("Luke", 100);
            char2.Print();           
        }
    }
}
