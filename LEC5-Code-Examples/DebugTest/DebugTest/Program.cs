﻿using System;

namespace DebugTest
{
    class Program
    {
        static void Main(string[] args)
        {
            bool continueGame = true;
            char iChar = '_';

            while (continueGame)
            {
                Console.Write("Move N, S, E, W: ");
                iChar = Console.ReadLine().ToUpper()[0];
                switch (iChar)
                {
                    case 'N':
                        Console.WriteLine("Code for move N executed now");
                        continue;
                    case 'S':
                        Console.WriteLine("Code for move S executed now");
                        break;
                    case 'E':
                        Console.WriteLine("Code for move E executed now");
                        break;
                    case 'W':
                        Console.WriteLine("Code for move W executed now");
                        continue;
                    default: 
                        Console.WriteLine("Invalid Entry");
                        continue;
                }
                Console.Write("Continue (Y/N): ");
                 iChar = Console.ReadLine().ToUpper()[0];
                if (iChar.Equals('N'))
                    continueGame = false;
            }
        }
    }
}
