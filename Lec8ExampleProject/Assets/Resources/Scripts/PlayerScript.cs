﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{ 
    private float move = 0.05f;
    private float pixelsToUnits;

    private float leftEdge;
    private float rightEdge;
    private float xPos;
    private float xSize;

    // Start is called before the first frame update
    void Start()
    {
        pixelsToUnits = gameObject.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;

        leftEdge = -((Screen.width * 0.5f) / pixelsToUnits);
        rightEdge = (Screen.width * 0.5f) / pixelsToUnits;

        xSize = gameObject.GetComponent<SpriteRenderer>().bounds.size.x * 0.5f;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            xPos = gameObject.transform.position.x;
            if (xPos - xSize > leftEdge)
            {
                gameObject.transform.position += Vector3.left * move;
            }
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            xPos = gameObject.transform.position.x;
            if (xPos + xSize < rightEdge)
            {
                gameObject.transform.position += Vector3.right * move;
            }
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            gameObject.transform.position += Vector3.down * move;
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
            gameObject.transform.position += Vector3.up * move;
        }
    }
}