﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    private float pixelsToUnits;

    // Start is called before the first frame update
    void Start()
    {
        pixelsToUnits = GameObject.Find("Player").GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
        Camera.main.orthographicSize = (Screen.height * 0.5f) / pixelsToUnits;
    }

}
