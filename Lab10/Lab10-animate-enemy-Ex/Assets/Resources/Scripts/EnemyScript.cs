﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum direction { east, west };

public class EnemyScript : MonoBehaviour
{
    direction currDirection;
    private float leftEdge;
    private float rightEdge;
    private float pixelsToUnits;
    private float moveDistance = 0.05f;
    private int loopCount = 0;
    private int loopMax = 5;

    // Start is called before the first frame update
    void Start()
    {
        // select random direction to start moving
        if (Random.Range(0, 2) == 0)
        {
            currDirection = direction.east;
        }
        else
        {
            currDirection = direction.west;
        }

        GameObject thePlayer = GameObject.FindGameObjectWithTag("Player");
        pixelsToUnits = thePlayer.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
        leftEdge = (Screen.width * -0.5f) / pixelsToUnits;
        rightEdge = (Screen.width * 0.5f) / pixelsToUnits;
    }

    // Update is called once per frame
    void Update()
    {
        // Version #1 to slow it down
        if (loopCount == loopMax)
        {
            moveEnemy();
            loopCount = 0;
        }
        else
            loopCount++;
    }

    void moveEnemy()
    {
        if (currDirection.Equals(direction.west))
        {
            if (gameObject.transform.position.x - (0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.x) > leftEdge)
            {
                gameObject.transform.position += Vector3.left * moveDistance;
            }
            else
            {
                currDirection = reverseDirection();
            }
        }
        else // assume it is east
        {
            if (gameObject.transform.position.x + (0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.x) < rightEdge)
            {
                gameObject.transform.position += Vector3.right * moveDistance;
            }
            else
            {
                currDirection = reverseDirection();
            }
        }
    }

    // code from Lab week 7
    private direction reverseDirection()
    {
        if (currDirection.Equals(direction.east))
            return direction.west;
        else
            return direction.east;
    }
}
