﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnEnemy : MonoBehaviour
{
    private GameObject theEnemy;

    void Start()
    {
        GameObject enemyPrefab = Resources.Load("Prefabs/Enemy") as GameObject;
        if (enemyPrefab != null)
            theEnemy = Instantiate(enemyPrefab);
        theEnemy.transform.position = new Vector3(1, 1, 0);
    }
}
