﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// (1) using directive for Tilemaps
using UnityEngine.Tilemaps;

public class BackgroundMapScript : MonoBehaviour
{
    // (2) set tile to use in Inspector
    public Tile backgroundTile;
    float pixelsToUnits;

    void Start()
    {
        // (3) Get pixelsToUnits and bounds of the world
        pixelsToUnits = GameObject.FindWithTag("Player").GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
        float leftX = -((Screen.width * 0.5f) / pixelsToUnits);
        float rightX = (Screen.width * 0.5f) / pixelsToUnits;
        float topY = (Screen.height * 0.5f / pixelsToUnits);
        float bottomY = -(Screen.height * 0.5f / pixelsToUnits);

    }
}
