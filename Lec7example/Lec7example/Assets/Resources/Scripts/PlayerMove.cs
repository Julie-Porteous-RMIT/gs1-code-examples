﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    Vector3 playerPos;
    Vector3 leftVec;

    private float moveDistance = 0.05f;
    private float pixelsToUnits;

    private float leftEdge;
    private float rightEdge;
    float xPos;
    float xSize;

    private void Awake()
    {
        pixelsToUnits = gameObject.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
    }

    // Start is called before the first frame update
    void Start()
    {
        //Camera.main.orthographicSize = (0.5f * Screen.height) / pixelsToUnits;

        leftEdge = -((0.5f * Screen.width) / pixelsToUnits);
        rightEdge = (0.5f * Screen.width) / pixelsToUnits;

        xSize = 0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void Update()
    {
        xPos = gameObject.transform.position.x;
        
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if ((xPos - xSize) > leftEdge)
            {
                leftVec = new Vector3(-1, 0, 0);
                gameObject.transform.position += leftVec * moveDistance;
            }
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            if ((xPos + xSize) < rightEdge)
            {
                gameObject.transform.position += Vector3.right * moveDistance;
            }
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            gameObject.transform.position += Vector3.down * moveDistance;
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
            gameObject.transform.position += Vector3.up * moveDistance;
        }
    }
}
