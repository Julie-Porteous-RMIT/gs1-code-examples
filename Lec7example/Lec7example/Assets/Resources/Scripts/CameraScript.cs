﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    private float pixelsToUnits;

    // Start is called before the first frame update
    void Start()
    {
        GameObject thePlayer = GameObject.Find("Player");
        pixelsToUnits = thePlayer.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;

        Camera.main.orthographicSize = (0.5f * Screen.height) / pixelsToUnits;
        //gameObject.GetComponent<Camera>().orthographicSize = (0.5f * Screen.height) / pixelsToUnits;
    }  
}
