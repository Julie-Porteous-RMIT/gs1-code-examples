﻿using System;
namespace RPGCharacterInheritanceExample
{
    public class Hero : RPGCharacter
    {
        private int _power;

        public Hero(string name, int health, int power)
        {
            Console.WriteLine($"Name {Name}");
            Name = name;
            Health = health;
            _power = power;
        }

        public int Power
        {
            get => _power;
            set => _power = value;
        }

        public void PowerUp()
        {
            _power += 10;
        }
    }
}
