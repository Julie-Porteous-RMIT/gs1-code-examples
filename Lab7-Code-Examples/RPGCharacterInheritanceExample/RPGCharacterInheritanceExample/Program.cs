﻿using System;

namespace RPGCharacterInheritanceExample
{
    class Program
    {
        static void Main(string[] args)
        {
            RPGCharacter char1 = new RPGCharacter();
            char1.Name = "Leia";
            char1.Health = 500;
            char1.Print();

            Hero char3 = new Hero("Luke", 500, 10);
            char3.Print();
            Console.WriteLine($"Char {char3.Name} has {char3.Power} power");


        }
    }
}
