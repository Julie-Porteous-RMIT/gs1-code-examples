﻿using System;
using System.Text;

namespace CharEx
{
    class Program
    {
        static void Main(string[] args)
        {
            // New variable with empty character
            char cVar1 = new char();
            Console.WriteLine($"cVar1 >>{cVar1}<<");

            char cVar2 = 'A';
            char cVar3 = '\u0041';
            Console.WriteLine($"cVar2 {cVar2} cVar3 {cVar3}");

            // Get the int value of A and output to console
            int aInt = (int)'A';
            Console.WriteLine($"aInt {aInt}");
        }
    }
}
