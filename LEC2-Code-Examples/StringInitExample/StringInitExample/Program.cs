﻿using System;
using System.Text;

namespace StringInitExample
{
    class Program
    {
        static void Main(string[] args)
        {
            // String Examples
            string sVar1 = String.Empty;
            Console.WriteLine($"sVar1 >>{sVar1}<<");

            // (1) Combining strings with string concatenation
            string sVar2 = "If at first you don't succeed ";
            string sVar3 = "try and ";
            string sVar4 = "try again";
            string sVar5 = sVar2 + sVar3 + sVar4;
            Console.WriteLine($"sVar5 >>{sVar5}<<");

            // (2) Combining Strings with StringBuilder
            StringBuilder sb = new StringBuilder();
            sb.Append(sVar2);
            sb.Append(sVar3);
            sb.Append("try again");
            Console.WriteLine($"sb >>{sb}<<");
        }
    }
}
