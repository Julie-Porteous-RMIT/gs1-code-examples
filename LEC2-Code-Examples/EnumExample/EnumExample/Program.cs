﻿using System;

namespace EnumExample
{
    // List of named integer constants
    enum suits { Hearts, Clubs, Spades, Diamonds };

    class Program
    {
        static void Main(string[] args)
        {
            // Can refer to a constant using its name
            suits trumpSuit = suits.Hearts;
            Console.WriteLine($"The trump suit is {trumpSuit}");

            /* Can use explicit cast to int to get integer value of
             * one of the named integer constants */
            int suitsInt = (int)suits.Hearts;
            // Or could use trumpSuit which is the same constant
            //int suitsInt = (int)trumpSuit;
            Console.WriteLine($"Integer val of Suit {trumpSuit} is {suitsInt}");
        }
    }
}
