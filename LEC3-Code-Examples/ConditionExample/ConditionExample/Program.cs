﻿using System;

namespace ConditionExample
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Ex 1: Have a go at writing code that:
             * Prompts the user to select an option from A, B or C
             * Use if-else -if to test output
             * Output some response to user
             */
            Console.Write("Please select A B or C: ");
            var iStr = Console.ReadLine().ToUpper();

            if (iStr.StartsWith("A"))
                Console.WriteLine("Valid Input");
            else if (iStr.StartsWith("B"))
                Console.WriteLine("Valid Input");
            else if (iStr.StartsWith("C"))
                Console.WriteLine("Valid Input");
            else
                Console.WriteLine("Invalid Input");

            /* EX 2: Have a go at writing code that:
             * Prompts the user to input a number
             * Tests whether it is even (HINT: use the % operator)
             * Output response to the user
             */

            Console.Write("Please enter number: ");

            var nStr = Convert.ToInt32(Console.ReadLine());
            if (nStr % 2 == 0)
            {
                Console.WriteLine("Even");
            }
            else
                Console.WriteLine("Odd");
        }
    }
}
