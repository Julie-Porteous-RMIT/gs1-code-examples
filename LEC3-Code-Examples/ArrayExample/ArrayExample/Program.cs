﻿using System;

namespace ArrayExample
{
    class Program
    {
        static void Main(string[] args)
        {
            // Example: declare array
            //char[] myGame = { '_', 'T', '_', '_', 'P', '_', '_', '_',  'H' };

            int width = 3;
            int height = 3;
            int gameArrayUpperBound = width * height;

            // Example: testing if moving north is valid ie cell in gameboard
            Console.Write("Enter player index position to test: ");
            int playerIndex = Convert.ToInt32(Console.ReadLine());

            // Move NORTH VALID?  
            int northIndex = playerIndex + width;
            //if (northIndex <= gameArrayUpperBound)
            if (northIndex < gameArrayUpperBound)
            {
                Console.WriteLine($"Valid move to {northIndex}");
            }
            else
                Console.WriteLine($"Invalid Move NORTH");

            // Example: testing if moving east is valid ie cell in gameboard
            // playerRow: 1D array to 2D game Board
            int playerRow = playerIndex / height;
            int eastUpperBound = (width * playerRow) + height - 1;
            int eastIndex = playerIndex + 1;

            if (playerIndex + 1 <= eastUpperBound)
                Console.WriteLine($"Valid move to {eastIndex}");
            else
                Console.WriteLine($"Invalid Move EAST");
        }
    }
}
