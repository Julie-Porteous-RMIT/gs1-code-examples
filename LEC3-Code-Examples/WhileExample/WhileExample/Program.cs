﻿using System;

namespace WhileExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var iStr = String.Empty;
            bool continueLoop = true;

            while (continueLoop)
            {
                Console.Write("Please select A B C or X: ");
                iStr = Console.ReadLine().ToUpper();

                if (iStr.StartsWith("A"))
                    Console.WriteLine("Valid Input");

                else if (iStr.StartsWith("B"))
                    Console.WriteLine("Valid Input");

                else if (iStr.StartsWith("C"))
                    Console.WriteLine("Valid Input");

                else if (iStr.Equals("X"))
                {
                    // EXIT ACTION
                    continueLoop = false;
                    Console.WriteLine("BYE! \r\n");
                }
                else
                    Console.WriteLine("Invalid Input");
            }

        }
    }
}
