﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnEnemy : MonoBehaviour
{
    private GameObject theEnemy;
    // Start is called before the first frame update
    void Start()
    {
        GameObject enemyPrefab = Resources.Load("Prefabs/Enemy") as GameObject;
        if (enemyPrefab != null)
            theEnemy = Instantiate(enemyPrefab);
        theEnemy.transform.position = new Vector3(1, 1, 0);
    }
}
