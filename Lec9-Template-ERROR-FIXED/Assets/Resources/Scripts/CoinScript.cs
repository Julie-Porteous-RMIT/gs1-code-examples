﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour
{
    private int score;

    // 1: public variable for sprites
    //public Sprite[] sprites;

    // 2: private array
    private Sprite[] sprites;

    private int currIndex = 0;
    private int maxIndex = 7;

    private int loopCount = 0;
    private int loopMax = 5;

    private void Start()
    {
        sprites = Resources.LoadAll<Sprite>("Artwork/coin_gold");
    }

    private void Update()
    {
        if (loopCount == loopMax)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = sprites[currIndex];
            if (currIndex < maxIndex)
            {
                currIndex++;
            }
            else
                currIndex = 0;
            loopCount = 0;
        }
        else
            loopCount++;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        score = GameObject.FindWithTag("Player").GetComponent<PlayerScript>().Score += 100;
        //print($"Score: {score}");
        Destroy(gameObject);
    }
}
