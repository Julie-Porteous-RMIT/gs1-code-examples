﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    private RectTransform scoreBox;
    private Text theText;
    private float pixelsToUnits = 100;

        // Start is called before the first frame update
    void Start()
    {
        scoreBox = gameObject.GetComponent<RectTransform>();
        Vector2 scoreSize = scoreBox.sizeDelta;

        float xPos = -(Screen.width * 0.5f) + (0.5f * scoreSize.x);
        float yPos = -(Screen.height * 0.5f) + (0.5f * scoreSize.y);

        scoreBox.localPosition = new Vector3(xPos, yPos, 0);
        theText = gameObject.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        theText.text = $"SCORE: {GameObject.FindWithTag("Player").GetComponent<PlayerScript>().Score}";
    }
}
