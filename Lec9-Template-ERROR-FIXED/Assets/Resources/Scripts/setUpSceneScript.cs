﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class setUpSceneScript : MonoBehaviour
{
    private float pixelsToUnits;
    private GameObject thePlayer;

    private int numCol;
    private int numRow;
    private float tileSize;
    private GameObject tile1;
    private GameObject tile2;
    private GameObject portalTile;


    void Awake()
    {
        thePlayer = Instantiate((GameObject)Resources.Load("Prefabs/Player"));

        pixelsToUnits = thePlayer.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
        Camera.main.orthographicSize = (Screen.height * 0.5f) / pixelsToUnits;

        float topLeftX = -((Screen.width * 0.5f) / pixelsToUnits) + thePlayer.GetComponent<SpriteRenderer>().sprite.bounds.size.x * 0.5f;
        float topLeftY = (Screen.height * 0.5f / pixelsToUnits) - thePlayer.GetComponent<SpriteRenderer>().sprite.bounds.size.y * 0.5f;
        thePlayer.transform.position = new Vector3(topLeftX, topLeftY, 0);
    }


    private void Start()
    {
        // Get size of tiles (assume square and all tiles the same size)
        portalTile = (GameObject)Resources.Load("Prefabs/BlackTile");
        tileSize = portalTile.GetComponent<SpriteRenderer>().sprite.bounds.size.x * pixelsToUnits;
         
        // get num of cols and rows
        numCol = (int)(Screen.width / tileSize + 1);
        numRow = (int)(Screen.height / tileSize + 1);

        // get the other tiles to use = tile1 and tile2
        tile1 = (GameObject)Resources.Load("Prefabs/OrangeTile");
        tile2 = (GameObject)Resources.Load("Prefabs/YellowTile");

        // Random Positions (col,row) for Portal tile
        int portalCol = Random.Range(1, numCol - 1);
        int portalRow = Random.Range(1, numRow - 1);

        float xPos;
        float yPos;
        int rand;
        GameObject theTile;

        // Loop through rows from 0
        for (int row = 0; row < numRow; row++)
        {
            // get Ypos
            yPos = (float)((Screen.height * 0.5f) - (tileSize * row) - (0.5f * tileSize)) / pixelsToUnits;
            // Loop through cols
            for (int col = 0; col < numCol; col++)
            {
                // get Xpos  and place tile
                xPos = (float)(-(0.5f * Screen.width) + (tileSize * col) + (0.5f * tileSize)) / pixelsToUnits;

                if (row == portalRow && col == portalCol)
                {
                    theTile = (GameObject)Instantiate(portalTile);
                }
                else
                {
                    rand = Random.Range(0, 2);
                    if (rand == 0)
                    {
                        theTile = (GameObject)Instantiate(tile1);
                    }
                    else
                    {
                        theTile = (GameObject)Instantiate(tile2);
                    }
                }
                theTile.transform.position = new Vector3(xPos, yPos, 0);
            }
        }
    }




}